@extends('layouts.app')
@section('title', 'National Center for Mental Health : Login')

@section('content')

<!-- Begin::Container -->
<img class="wave" src="{{ asset('images/wave.png') }}">
<div class="container">
    <div class="img">
        <img src="{{ asset('images/bg.svg') }}">
    </div>
    <div class="login-content">
        {{ Form::open(array('route' => 'login.index', 'method' => 'POST','files' => 'true')) }}
            <img src="{{ asset('images/ncmh_logo.png') }}">
            <h2 class="title">Welcome</h2>
                <div class="input-div one">
                    <div class="i">
                            <i class="fas fa-user"></i>
                    </div>
                    <div class="div">
                            <h5>Username</h5>
                            {{ Form::text('username', null, ['class' => 'input', 'required' => 'required']) }}
                    </div>
                </div>
                <div class="input-div pass">
                    <div class="i"> 
                        <i class="fas fa-lock"></i>
                    </div>
                    <div class="div">
                        <h5>Password</h5>
                        {{ Form::password('password', array('class'=>'input', 'required'=> 'required' ) ) }}
                </div>
            </div>
            <a href="#">Forgot Password?</a>
            {{ Form::submit('Login', ['class'=>'btn', 'name'=>'submit']) }}
        {{ Form::close() }}
    </div>
</div>
<!-- End::Container -->

@endsection
