@extends('layouts.pageApp')
@section('title', 'National Center for Mental Health : Home')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <nav class="navbar navbar-expand-lg" style="background-color: lightgray;">
                    <div class="container-fluid">
                        HELLO WORLD
                        <div class="collapse navbar-collapse">
                            <ul class="nav ml-auto">
                                <li class="nav-item">
                                    <button class="btn btn-primary" onclick="hn_hopl_view()">View</button>
                                </li>
                                <li class="nav-item">
                                    <button class="btn btn-primary" data-toggle="modal" data-target="#hoplModal">Add</button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <div class="container-fluid" id="hn_hopl_records">
                    <table id="hn_hopl_table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th width="10%">Field A</th>
                                <th width="10%">Field B</th>
                                <th width="80%">Field C</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Hello</td>
                                <td>World</td>
                                <td>
                                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Soluta nam quisquam dolorum minus, sunt cum animi suscipit fugiat labore, eos obcaecati ipsum tenetur cupiditate quod pariatur iusto laboriosam fugit? Accusamus!</p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="modal fade" id="hoplModal">
                    <div class="modal-dialog">
                        <div class="modal-content">
    
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="modal-title">HELLO WORLD</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
    
                            <!-- Modal body -->
                            <div class="modal-body">
                                {{ Form::open(array('route' => 'home.index', 'method' => 'POST','files' => 'true')) }}
                                   
                                    {{ Form::label('hn_hopl_onset','Field A:', ['class' => 'font-weight-bold', 'style'=>'letter-spacing: 0px']) }}
                                    {{ Form::text('hn_hopl_onset', null, ['class' => 'form-control mb-2', 'placeholder'=>'Date/Year', 'required'=>'required']) }}

                                    {{ Form::label('hn_hopl_recent','Field B:', ['class' => 'font-weight-bold', 'style'=>'letter-spacing: 0px']) }}
                                    {{ Form::text('hn_hopl_recent', null, ['class' => 'form-control mb-2', 'placeholder'=>'Date/Year', 'required'=>'required']) }}

                                    {{ Form::label('hn_hopl_narrative','Field C:', ['class' => 'font-weight-bold', 'style'=>'letter-spacing: 0px']) }}
                                    {{ Form::textarea('hn_hopl_narrative', null, ['class' => 'form-control mb-2', 'placeholder'=>'Summary', 'required'=>'required']) }}

                                    {{ Form::submit('Submit', ['class'=>'btn btn-block btn-outline-primary']) }}

                                {{ Form::close() }}
                            </div>
    
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#hn_hopl_records').slideUp();
        });

        function hn_hopl_view() {
            $('#hn_hopl_records').slideToggle();
        };

    </script>

@endsection