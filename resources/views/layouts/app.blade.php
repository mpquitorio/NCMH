<!DOCTYPE html>
<!-- Begin::HTML -->
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <!-- Begin::Head -->
    <head>
        <!-- Meta -->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="author" content="National Center for Mental Health">
        <meta name="description" content="National Center For Mental Health: Login Code Challenge using PHP Laravel Framework">

        <!-- Title -->
        <title>@yield('title')</title>

        <!-- Fontawesome -->
        <script src="https://kit.fontawesome.com/54dd48c648.js" crossorigin="anonymous"></script>
        <!-- Custom CSS -->
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        
    </head>
    <!-- End::Head -->
    <!-- Begin::Body -->
    <body>
        @yield('content')
        <script src="{{ asset('js/script.js') }}"></script>
    </body>
    <!-- End::Body -->
</html>
<!-- End::HTML -->